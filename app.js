// server dependecies
	const express = require("express")
	const mongoose = require("mongoose")
	const taskRoutes = require("./routes/taskRoutes")

// initialize env file
	require("dotenv").config()

// server setup
	const app = express()
	const port = 3001

// mongodb/mongoose connection
	mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.6sl1kgl.mongodb.net/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`, 
		{
			useNewUrlParser:true,
			useUnifiedTopology:true
		})

// event listeners
	let db = mongoose.connection
	db.on("error", () => console.log(`Connection error`))
	db.once("open", () => console.log(`Connected to MongoDB!`))


// middleware registration
	app.use(express.json())
	app.use(express.urlencoded({extended:true}))

// Routes
	app.use("/tasks", taskRoutes)

// server listening
	app.listen(port, () => console.log(`The server is running at localhost:${port}`))