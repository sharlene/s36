// import express
	const express = require("express")
	const router = express.Router()

// import controller
	const TaskController = require("../controllers/TaskController")

// routes
	// get all tasks
		router.get("/", (request, response) => {
			TaskController.getAll().then(result => response.send(result))
		})

	// create new task 
		router.post("/", (request, response) => {
			TaskController.createTask(request.body).then(result => response.send(result))
		})

	// get specific task
		router.get("/:id", (request, response) => {
			TaskController.updateTask(request.params.id, request.body).then(result => response.send(result))
		})

	// update existing task
		router.put("/:id", (request, response) => {
			TaskController.updateTask(request.params.id, request.body).then(result => response.send(result))
		})

	// update existing task status
		router.put("/:id/completed", (request, response) => {
			TaskController.updateTask(request.params.id, request.body).then(result => response.send(result))
		})

	// delete existing task
		router.delete("/:id", (request, response) => {
			TaskController.deleteTask(request.params.id, request.body).then(result => response.send(result))
		})

// export routes
module.exports = router