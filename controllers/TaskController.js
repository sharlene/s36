// import task
const Task = require("../models/Task")

// export module
	// get all
	module.exports.getAll = () => {
		return Task.find({}).then(result => {return result})
	}

	// get specific task
	module.exports.getTask = (task_id, new_content) => {
		return Task.findById(task_id).then((result, error) => {return result})
	}

	// create task
	module.exports.createTask = (request_body) => {
		let new_task = new Task({
			name: request_body.name
		})

		return new_task.save().then((created_task, error)=> {
			if(error){
				return error
			}

			return created_task 
		})
	}

	// update task
	module.exports.updateTask = (task_id, new_content) => {
		return Task.findById(task_id).then((result, error) => {
			if(error){
				return error
			}
			result.name = new_content.name

			return result.save().then((updated_task, error)=>{
				if(error){
					return error
				}

				return updated_task
			})
		})
	}

	// update task status
	module.exports.updateTask = (task_id, new_content) => {
		return Task.findById(task_id).then((result, error) => {
			if(error){
				return error
			}
			result.status = new_content.status

			return result.save().then((updated_task, error)=>{
				if(error){
					return error
				}

				return updated_task
			})
		})
	}
	// delete task
	module.exports.deleteTask = (task_id) => {
		return Task.findByIdAndRemove(task_id).then((deleted_task, error)=> {
			if(error){
				return error
			}
			return deleted_task
		})
	}