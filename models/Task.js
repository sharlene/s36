// import mongoose
const mongoose = require("mongoose")

// task schema
const task_schema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default:"Pending"
	}
})

// module export
module.exports = mongoose.model("Task", task_schema)